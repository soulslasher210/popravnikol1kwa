import { Component, OnInit } from '@angular/core';
import { stavkaRacun } from './stavkaRacun';
import { OcenaService } from './ocena.service';

@Component({
  selector: 'app-ocena',
  templateUrl: './ocena.component.html',
  styleUrls: ['./ocena.component.css']
})
export class OcenaComponent implements OnInit {

  ocene: stavkaRacun[];

  constructor(private ocenaService: OcenaService) {}

  ngOnInit(){
    this.getOcena();
  }

  getOcena(): void {
    this.ocenaService.getOcena().subscribe(ocene => this.ocene = ocene);
  }

  add(racunId: string): void {
    racunId = racunId.trim();
    if (!racunId) { return; }
    this.ocenaService.addOcena({ racunId } as stavkaRacun)
      .subscribe(ocena => {
        this.ocene.push(ocena);
      });
  }

  delete(ocena: stavkaRacun): void {
    this.ocene = this.ocene.filter(h => h !== ocena);
    this.ocenaService.deleteOcena(ocena).subscribe();
  }
}

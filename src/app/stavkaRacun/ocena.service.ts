import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { stavkaRacun } from './stavkaRacun';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OcenaService {

  private ocenaUrl = 'api/ocena'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    ) { }

  getOcena(): Observable<stavkaRacun[]> {
    return this.http.get<stavkaRacun[]>(this.ocenaUrl)
      .pipe(
        catchError(this.handleError<stavkaRacun[]>('getOcena', []))
      );
  }

  getOcenakNo404<Data>(id: number): Observable<stavkaRacun> {
    const url = `${this.ocenaUrl}/?id=${id}`;
    return this.http.get<stavkaRacun[]>(url)
      .pipe(
        map(ocene => ocene[0]),
        tap(h => {
          const outcome = h ? 'fetched' : 'did not find';
        }),
        catchError(this.handleError<stavkaRacun>('getOce id=${id}'))
      );
  }

  getOce(id: number): Observable<stavkaRacun> {
    const url = `${this.ocenaUrl}/${id}`;
    return this.http.get<stavkaRacun>(url).pipe(
      catchError(this.handleError<stavkaRacun>(`getOce id=${id}`))
    );
  }

  addOcena(ocena: stavkaRacun): Observable<stavkaRacun> {
    return this.http.post<stavkaRacun>(this.ocenaUrl, ocena, this.httpOptions).pipe(
      catchError(this.handleError<stavkaRacun>('addOcena'))
    );
  }

  deleteOcena(ocena: stavkaRacun | number): Observable<stavkaRacun> {
    const id = typeof ocena === 'number' ? ocena : ocena.id;
    const url = `${this.ocenaUrl}/${id}`;

    return this.http.delete<stavkaRacun>(url, this.httpOptions).pipe(
      catchError(this.handleError<stavkaRacun>('deleteOcena'))
    );
  }

  updateOcena(ocena: stavkaRacun): Observable<any> {
    return this.http.put(this.ocenaUrl, ocena, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateOcena'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}

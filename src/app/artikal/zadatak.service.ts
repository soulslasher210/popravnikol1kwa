import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Zadatak } from './zad';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({providedIn: 'root'})
export class ZadatakService {

  private zadatakUrl = 'api/zadatak'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    ) { }

  getZadatak(): Observable<Zadatak[]> {
    return this.http.get<Zadatak[]>(this.zadatakUrl)
      .pipe(
        catchError(this.handleError<Zadatak[]>('getZadatak', []))
      );
  }

  getZadatakNo404<Data>(id: number): Observable<Zadatak> {
    const url = `${this.zadatakUrl}/?id=${id}`;
    return this.http.get<Zadatak[]>(url)
      .pipe(
        map(zadaci => zadaci[0]),
        tap(h => {
          const outcome = h ? 'fetched' : 'did not find';
        }),
        catchError(this.handleError<Zadatak>('getZad id=${id}'))
      );
  }

  getZad(id: number): Observable<Zadatak> {
    const url = `${this.zadatakUrl}/${id}`;
    return this.http.get<Zadatak>(url).pipe(
      catchError(this.handleError<Zadatak>(`getZad id=${id}`))
    );
  }

  addZadatak(zadatak: Zadatak): Observable<Zadatak> {
    return this.http.post<Zadatak>(this.zadatakUrl, zadatak, this.httpOptions).pipe(
      catchError(this.handleError<Zadatak>('addZadatak'))
    );
  }

  deleteZadatak(zadatak: Zadatak | number): Observable<Zadatak> {
    const id = typeof zadatak === 'number' ? zadatak : zadatak.id;
    const url = `${this.zadatakUrl}/${id}`;

    return this.http.delete<Zadatak>(url, this.httpOptions).pipe(
      catchError(this.handleError<Zadatak>('deleteZadatak'))
    );
  }

  updateZadatak(zadatak: Zadatak): Observable<any> {
    return this.http.put(this.zadatakUrl, zadatak, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateZadatak'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}


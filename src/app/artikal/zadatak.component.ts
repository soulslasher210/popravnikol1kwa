import { Component, OnInit } from '@angular/core';
import { Zadatak } from './zad';
import { ZadatakService } from './zadatak.service';

@Component({
  selector: 'app-zadatak',
  templateUrl: './zadatak.component.html',
  styleUrls: ['./zadatak.component.css']
})
export class ZadatakComponent implements OnInit {

  zadaci: Zadatak[];

  constructor(private zadatakService: ZadatakService) {}

  ngOnInit(){
    this.getZadatak();
  }

  getZadatak(): void {
    this.zadatakService.getZadatak().subscribe(zadaci => this.zadaci = zadaci);
  }

  add(naziv: string): void {
    naziv = naziv.trim();
    if (!naziv) { return; }
    this.zadatakService.addZadatak({ naziv } as Zadatak)
      .subscribe(zadatak => {
        this.zadaci.push(zadatak);
      });
  }

  delete(zadatak: Zadatak): void {
    this.zadaci = this.zadaci.filter(h => h !== zadatak);
    this.zadatakService.deleteZadatak(zadatak).subscribe();
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { stavkaRacun } from '../stavkaRacun/stavkaRacun';
import { OcenaService }  from '../stavkaRacun/ocena.service';

@Component({
  selector: 'app-ocena-detail',
  templateUrl: './ocena-detail.component.html',
  styleUrls: ['./ocena-detail.component.css']
})
export class OcenaDetailComponent implements OnInit {
  @Input() ocena: stavkaRacun;

  constructor(
    private route: ActivatedRoute,
    private ocenaService: OcenaService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getOce();
  }

  getOce(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.ocenaService.getOce(id).subscribe(ocena => this.ocena = ocena);
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    this.ocenaService.updateOcena(this.ocena).subscribe(() => this.goBack());
  }
}


import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ZadatakComponent } from './artikal/zadatak.component';
import { ZadatakDetailComponent }  from './artikal-detail/zadatak-detail.component';

import { OcenaComponent } from './stavkaRacun/ocena.component';
import { OcenaDetailComponent }  from './stavkaRacun-detail/ocena-detail.component';

import { StudentComponent } from './racun/student.component';
import { StudentDetailComponent }  from './racun-detail/student-detail.component';

const routes: Routes = [
  { path: 'detail/:id', component: ZadatakDetailComponent },
  { path: 'artikal', component: ZadatakComponent },
  { path: 'detaill/:id', component: OcenaDetailComponent },
  { path: 'stavkaRacun', component: OcenaComponent },
  { path: 'detailll/:id', component: StudentDetailComponent },
  { path: 'racun', component: StudentComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

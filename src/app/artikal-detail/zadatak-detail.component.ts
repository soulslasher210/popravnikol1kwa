import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Zadatak } from '../artikal/zad';
import { ZadatakService }  from '../artikal/zadatak.service';


@Component({
  selector: 'app-zadatak-detail',
  templateUrl: './zadatak-detail.component.html',
  styleUrls: ['./zadatak-detail.component.css']
})
export class ZadatakDetailComponent implements OnInit {
  @Input() zadatak: Zadatak;

  constructor(
    private route: ActivatedRoute,
    private zadatakService: ZadatakService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getZad();
  }

  getZad(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.zadatakService.getZad(id).subscribe(zadatak => this.zadatak = zadatak);
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    this.zadatakService.updateZadatak(this.zadatak).subscribe(() => this.goBack());
  }
}

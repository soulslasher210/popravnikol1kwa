import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZadatakComponent } from './artikal/zadatak.component';
import { ZadatakDetailComponent } from './artikal-detail/zadatak-detail.component';
import { OcenaComponent } from './stavkaRacun/ocena.component';
import { OcenaDetailComponent } from './stavkaRacun-detail/ocena-detail.component';
import { StudentComponent } from './racun/student.component';
import { StudentDetailComponent } from './racun-detail/student-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ZadatakComponent,
    ZadatakDetailComponent,
    OcenaComponent,
    OcenaDetailComponent,
    StudentComponent,
    StudentDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,


    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

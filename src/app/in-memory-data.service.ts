import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Zadatak } from './artikal/zad';
import { stavkaRacun } from './stavkaRacun/stavkaRacun';
import { Racun } from './racun/racun';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{
  createDb() {
    const zadatak = [
      {
        "id": 1,
        "naziv": "Hleb",
        "cena": 45.5
     },
     {
        "id": 2,
        "naziv": "Mleko",
        "cena": 78.39
     },
     {
        "id": 3,
        "naziv": "So",
        "cena": 40.0
     },
     {
        "id": 4,
        "naziv": "Voda",
        "cena": 25.0
     }
    ];
    const ocena = [
      {
        "id": 1,
        "racunId": 1,
        "artikalId": 1,
        "cena": 40.0,
        "kolicina": 1
     },
     {
        "id": 2,
        "racunId": 1,
        "artikalId": 2,
        "cena": 80.5,
        "kolicina": 2
     },
     {
        "id": 3,
        "racunId": 1,
        "artikalId": 3,
        "cena": 45.0,
        "kolicina": 1
     },
     {
        "id": 4,
        "racunId": 3,
        "artikalId": 2,
        "cena": 78.39,
        "kolicina": 1
     }
    ];
    const student = [
      {
        "id": 1,
        "datumRacuna": "2020-01-06"
     },
     {
        "id": 2,
        "datumRacuna": "2020-02-16"
     },
     {
        "id": 3,
        "datumRacuna": "2020-02-03"
     },
     {
        "id": 4,
        "datumRacuna": "2020-03-26"
     }
    ];
    return {zadatak,ocena,student};
  }
  constructor() { }

  genIdO(ocena: stavkaRacun[]): number {
    return ocena.length > 0 ? Math.max(...ocena.map(ocena => ocena.id)) + 1 : 11;
  }

  genIdS(student: Racun[]): number {
    return student.length > 0 ? Math.max(...student.map(student => student.id)) + 1 : 11;
  }

  genId(zadatak: Zadatak[]): number {
    return zadatak.length > 0 ? Math.max(...zadatak.map(zadatak => zadatak.id)) + 1 : 11;
  }
}

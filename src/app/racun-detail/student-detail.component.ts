import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Racun } from '../racun/racun';
import { StudentService }  from '../racun/student.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {
  @Input() racun: Racun;

  constructor(
    private route: ActivatedRoute,
    private studentService: StudentService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getStu();
  }

  getStu(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.studentService.getStu(id).subscribe(student => this.racun = student);
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    this.studentService.updateStudent(this.racun).subscribe(() => this.goBack());
  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Racun } from './racun';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private studentUrl = 'api/student'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    ) { }

  getStudent(): Observable<Racun[]> {
    return this.http.get<Racun[]>(this.studentUrl)
      .pipe(
        catchError(this.handleError<Racun[]>('getStudent', []))
      );
  }

  getStudentNo404<Data>(id: number): Observable<Racun> {
    const url = `${this.studentUrl}/?id=${id}`;
    return this.http.get<Racun[]>(url)
      .pipe(
        map(studenti => studenti[0]),
        tap(h => {
          const outcome = h ? 'fetched' : 'did not find';
        }),
        catchError(this.handleError<Racun>('getStu id=${id}'))
      );
  }

  getStu(id: number): Observable<Racun> {
    const url = `${this.studentUrl}/${id}`;
    return this.http.get<Racun>(url).pipe(
      catchError(this.handleError<Racun>(`getStu id=${id}`))
    );
  }

  addStudent(student: Racun): Observable<Racun> {
    return this.http.post<Racun>(this.studentUrl, student, this.httpOptions).pipe(
      catchError(this.handleError<Racun>('addStudent'))
    );
  }

  deleteStudent(student: Racun | number): Observable<Racun> {
    const id = typeof student === 'number' ? student : student.id;
    const url = `${this.studentUrl}/${id}`;

    return this.http.delete<Racun>(url, this.httpOptions).pipe(
      catchError(this.handleError<Racun>('deleteStudent'))
    );
  }

  updateStudent(student: Racun): Observable<any> {
    return this.http.put(this.studentUrl, student, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateStudent'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}

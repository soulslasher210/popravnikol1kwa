import { Component, OnInit } from '@angular/core';
import { Racun } from './racun';
import { StudentService } from './student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  studenti: Racun[];

  constructor(private studentService: StudentService) {}

  ngOnInit(){
    this.getStudent();
  }

  getStudent(): void {
    this.studentService.getStudent().subscribe(studenti => this.studenti = studenti);
  }

  add(datumRacuna: string): void {
    datumRacuna = datumRacuna.trim();
    if (!datumRacuna) { return; }
    this.studentService.addStudent({ datumRacuna } as Racun)
      .subscribe(student => {
        this.studenti.push(student);
      });
  }

  delete(student: Racun): void {
    this.studenti = this.studenti.filter(h => h !== student);
    this.studentService.deleteStudent(student).subscribe();
  }

}
